﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scoreboard.Models
{
  public class Event
  {
    public string Name { get; set; }
    public int Id { get; set; }
    public string Weapons { get; set; }
    public string Attachments { get; set; }
    public string Mode { get; set; }
    public string Rules { get; set; }
    public DateTime Created { get; set; }
  }
}
