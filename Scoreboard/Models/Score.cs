﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scoreboard.Models
{
  public class Score
  {
    public int Id { get; set; }
    public Hero Hero { get; set; }
    public Event Event { get; set; }
    public int ScoreGot { get; set; }
    public DateTime Created { get; set; }
  }
}
