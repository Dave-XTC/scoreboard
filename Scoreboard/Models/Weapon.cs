﻿using System;

namespace Scoreboard.Models
{
  public class Weapon
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
  }
}
