﻿using System;

namespace Scoreboard.Models
{
  public class Attachment
  {
    public int Id { get; set; }
    public string Name { get; set; }
  }
}
