﻿using System;

namespace Scoreboard.Models.DTO
{
  public class ScoreDTO
  {
    public int Id { get; set; }
    public int HeroId { get; set; }
    public int EventId { get; set; }
    public int ScoreGot { get; set; }
    public DateTime Created { get; set; }
  }
}
