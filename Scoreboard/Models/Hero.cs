﻿using System;

namespace Scoreboard.Models
{
  public class Hero
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime Created { get; set; }
  }
}
