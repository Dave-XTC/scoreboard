﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scoreboard.Context;
using Scoreboard.Models;

namespace Scoreboard.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class WeaponsController : ControllerBase
  {

    private readonly ILogger<WeaponsController> _logger;

    public WeaponsController(ILogger<WeaponsController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IActionResult Get(int? id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        if (id.HasValue)
        {
          var weapon = scoreboardContext.Weapons.Where(h => h.Id == id).FirstOrDefault();
          return Ok(weapon);
        }

        var weapons = scoreboardContext.Weapons.ToList();
        return Ok(weapons);
      }
    }

    [HttpPost]
    public IActionResult Post([FromBody] Weapon weapon)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        scoreboardContext.Weapons.Add(weapon);
        scoreboardContext.SaveChanges();
        return Ok();
      }
    }

    [HttpPatch]
    public IActionResult Patch([FromBody] Weapon weapon)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingWeapon = scoreboardContext.Weapons.Where(h => h.Id == weapon.Id).FirstOrDefault();

        if (existingWeapon != null)
        {
          existingWeapon.Name = weapon.Name;
          existingWeapon.Type = weapon.Type;
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

    [HttpDelete]
    public IActionResult Delete(int id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingWeapon = scoreboardContext.Weapons.Where(h => h.Id == id).FirstOrDefault();

        if (existingWeapon != null)
        {
          scoreboardContext.Weapons.Remove(existingWeapon);
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

  }
}
