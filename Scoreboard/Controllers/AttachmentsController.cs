﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scoreboard.Context;
using Scoreboard.Models;

namespace Scoreboard.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class AttachmentsController : ControllerBase
  {

    private readonly ILogger<AttachmentsController> _logger;

    public AttachmentsController(ILogger<AttachmentsController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IActionResult Get(int? id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        if (id.HasValue)
        {
          var attachment = scoreboardContext.Attachments.Where(h => h.Id == id).FirstOrDefault();
          return Ok(attachment);
        }

        var attachments = scoreboardContext.Attachments.ToList();
        return Ok(attachments);
      }
    }

    [HttpPost]
    public IActionResult Post([FromBody] Attachment attachment)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        scoreboardContext.Attachments.Add(attachment);
        scoreboardContext.SaveChanges();
        return Ok();
      }
    }

    [HttpPatch]
    public IActionResult Patch([FromBody] Attachment attachment)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingAttachment = scoreboardContext.Attachments.Where(h => h.Id == attachment.Id).FirstOrDefault();

        if (existingAttachment != null)
        {
          existingAttachment.Name = attachment.Name;
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

    [HttpDelete]
    public IActionResult Delete(int id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingAttachment = scoreboardContext.Attachments.Where(h => h.Id == id).FirstOrDefault();

        if (existingAttachment != null)
        {
          scoreboardContext.Attachments.Remove(existingAttachment);
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

  }
}
