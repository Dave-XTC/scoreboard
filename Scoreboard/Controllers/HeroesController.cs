﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Scoreboard.Context;
using Scoreboard.Models;

namespace Scoreboard.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class HeroesController : ControllerBase
  {
    private readonly ILogger<HeroesController> _logger;

    public HeroesController(ILogger<HeroesController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IActionResult Get(int? id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        if (id.HasValue)
        {
          var hero = scoreboardContext.Heroes.Where(h => h.Id == id).FirstOrDefault();
          return Ok(hero);
        }

        var heroes = scoreboardContext.Heroes.ToList();
        return Ok(heroes);
      }
    }

    [HttpPost]
    public IActionResult Post([FromBody] Hero hero)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        hero.Created = DateTime.Now;
        scoreboardContext.Heroes.Add(hero);
        scoreboardContext.SaveChanges();
        return Ok();
      }
    }

    [HttpPatch]
    public IActionResult Patch([FromBody] Hero hero)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingHero = scoreboardContext.Heroes.Where(h => h.Id == hero.Id).FirstOrDefault();

        if (existingHero != null)
        {
          existingHero.Name = hero.Name;
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }
    [HttpDelete]
    public IActionResult Delete(int id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingHero = scoreboardContext.Heroes.Where(h => h.Id == id).FirstOrDefault();

        if (existingHero != null && IsSafeToDelete(existingHero.Id))
        {
          scoreboardContext.Heroes.Remove(existingHero);
          scoreboardContext.SaveChanges();
          return Ok();
        }
        else
        {
          return BadRequest("Hero has existing scores.");
        }
      }
      
    }

    private bool IsSafeToDelete(int heroId)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        return !scoreboardContext.Scores
          .Include(he => he.Hero)
          .Any(s => s.Hero.Id == heroId);
      }
    }

  }
}
