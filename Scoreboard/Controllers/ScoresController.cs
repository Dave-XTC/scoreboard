﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Scoreboard.Context;
using Scoreboard.Models;
using Scoreboard.Models.DTO;

namespace Scoreboard.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ScoresController : ControllerBase
  {

    private readonly ILogger<ScoresController> _logger;

    public ScoresController(ILogger<ScoresController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IActionResult Get(int? id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        if (id.HasValue)
        {
          var score = scoreboardContext.Scores.Where(h => h.Id == id)
            .Include(he => he.Hero)
            .Include(ev => ev.Event)
            .FirstOrDefault();

          return Ok(score);
        }

        var scores = scoreboardContext.Scores
          .Include(he => he.Hero)
          .Include(ev => ev.Event)
          .ToList();

        return Ok(scores);
      }
    }

    [HttpPost]
    public IActionResult Post([FromBody] ScoreDTO score)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        score.Created = DateTime.Now;
        scoreboardContext.Scores.Add(
          new Score()
          {
            Event = scoreboardContext.Events
              .Where(ev => ev.Id == score.EventId)
              .First(),
            Hero = scoreboardContext.Heroes
              .Where(he => he.Id == score.HeroId)
              .First(),
            ScoreGot = score.ScoreGot,
            Created = score.Created,
          });
        scoreboardContext.SaveChanges();
        return Ok();
      }
    }

    [HttpPatch]
    public IActionResult Patch([FromBody] ScoreDTO score)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingScore = scoreboardContext.Scores.Where(h => h.Id == score.Id).FirstOrDefault();

        if (existingScore != null)
        {
          existingScore.Hero = scoreboardContext.Heroes
              .Where(he => he.Id == score.EventId)
              .First();
          existingScore.Event = scoreboardContext.Events
              .Where(ev => ev.Id == score.EventId)
              .First();
          existingScore.ScoreGot = score.ScoreGot;
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

    [HttpDelete]
    public IActionResult Delete(int id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingScore = scoreboardContext.Scores.Where(h => h.Id == id).FirstOrDefault();

        if (existingScore != null)
        {
          scoreboardContext.Scores.Remove(existingScore);
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

  }
}
