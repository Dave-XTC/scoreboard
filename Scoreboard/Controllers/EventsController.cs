﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scoreboard.Context;
using Scoreboard.Models;

namespace Scoreboard.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class EventsController : ControllerBase
  {

    private readonly ILogger<EventsController> _logger;

    public EventsController(ILogger<EventsController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IActionResult Get(int? id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        if (id.HasValue)
        {
          var run = scoreboardContext.Events.Where(h => h.Id == id).FirstOrDefault();
          return Ok(run);
        }

        var runs = scoreboardContext.Events.ToList();
        return Ok(runs);
      }
    }

    [HttpPost]
    public IActionResult Post([FromBody] Event run)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        run.Created = DateTime.Now;
        scoreboardContext.Events.Add(run);
        scoreboardContext.SaveChanges();
        return Ok();
      }
    }

    [HttpPatch]
    public IActionResult Patch([FromBody] Event run)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingRun = scoreboardContext.Events.Where(h => h.Id == run.Id).FirstOrDefault();

        if (existingRun != null)
        {
          existingRun.Name = run.Name;
          existingRun.Weapons = run.Weapons;
          existingRun.Attachments = run.Attachments;
          existingRun.Mode = run.Mode;
          existingRun.Rules = run.Rules;
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

    [HttpDelete]
    public IActionResult Delete(int id)
    {
      using (var scoreboardContext = new ScoreboardContext())
      {
        var existingRun = scoreboardContext.Events.Where(h => h.Id == id).FirstOrDefault();

        if (existingRun != null)
        {
          scoreboardContext.Events.Remove(existingRun);
          scoreboardContext.SaveChanges();
        }

        return Ok();
      }
    }

  }
}
