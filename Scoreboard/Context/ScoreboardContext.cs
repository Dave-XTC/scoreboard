﻿using Microsoft.EntityFrameworkCore;
using Scoreboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scoreboard.Context
{
  public class ScoreboardContext : DbContext
  {
    public DbSet<Hero> Heroes { get; set; }
    public DbSet<Event> Events { get; set; }
    public DbSet<Score> Scores { get; set; }
    public DbSet<Weapon> Weapons { get; set; }
    public DbSet<Attachment> Attachments { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlServer(@"Data Source=davidickert.com;Initial Catalog=Scoreboard;User ID=Hibbafrab;Password=blue");
  }
}
