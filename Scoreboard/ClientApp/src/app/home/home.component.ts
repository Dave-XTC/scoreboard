import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent
{
  heroes: Hero[];
  events: Event[];
  scores: Score[];
  fetchRecent = 10;
  sortedScores: Score[];
  sortedColumn: string;
  sortDirection: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string)
  {
    http.get<Hero[]>(baseUrl + 'heroes').subscribe(result => { this.heroes = result; }, error => console.error(error));
    http.get<Event[]>(baseUrl + 'events').subscribe(result => { this.events = result; }, error => console.error(error));
    http.get<Score[]>(baseUrl + 'scores').subscribe(result => { this.scores = result; }, error => console.error(error));
    http.get<Score[]>(baseUrl + 'scores').subscribe(result => { this.sortedScores = result; }, error => console.error(error));
    this.sortedColumn = "" as string;
    this.sortDirection = "asc" as string;
  }

  fetchRecentScores(): Score[]
  {
    let recentScores = [];
    for (let i = 0; i < this.fetchRecent; i++)
    {
      recentScores.push(this.scores[this.scores.length - (1 + i)] as Score);
    }
    return recentScores as Score[];
  }

  calcHighScoreHolder(runType: Event) : Score
  {
    let tempScores = [];
    let tempNumbers = [];
    for (var i of this.scores)
    {
      if (i.event.id == runType.id)
      {
        tempScores.push(i);
        tempNumbers.push(i.scoreGot);
      }
    }
    var highScore: number;
    highScore = Math.max(...tempNumbers)
    for (let i = 0; i < tempScores.length; i++)
    {
      if (tempNumbers[i] == highScore)
      {
        return tempScores[i];
      }
    }
    return this.scores[1];
  }

  sortByColumn(type: string)
  {
    if (type == "event")
    {
      this.sortedColumn = type;
      if (this.sortDirection == "asc")
      {
        this.sortDirection = "des";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.event.name > n2.event.name) {
            return 1;
          }

          if (n1.event.name < n2.event.name) {
            return -1;
          }

          return 0;
        });
        return;
      }
      if (this.sortDirection == "des") {
        this.sortDirection = "asc";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.event.name < n2.event.name) {
            return 1;
          }

          if (n1.event.name > n2.event.name) {
            return -1;
          }

          return 0;
        });
        return;
      }
    }

    if (type == "hero")
    {
      this.sortedColumn = type;
      if (this.sortDirection == "asc") {
        this.sortDirection = "des";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.hero.name > n2.hero.name) {
            return 1;
          }

          if (n1.hero.name < n2.hero.name) {
            return -1;
          }

          return 0;
        });
        return;
      }
      if (this.sortDirection == "des") {
        this.sortDirection = "asc";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.hero.name < n2.hero.name) {
            return 1;
          }

          if (n1.hero.name > n2.hero.name) {
            return -1;
          }

          return 0;
        });
        return;
      }
    }

    if (type == "score")
    {
      this.sortedColumn = type;
      if (this.sortDirection == "asc") {
        this.sortDirection = "des";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.scoreGot > n2.scoreGot) {
            return 1;
          }

          if (n1.scoreGot < n2.scoreGot) {
            return -1;
          }

          return 0;
        });
        return;
      }
      if (this.sortDirection == "des") {
        this.sortDirection = "asc";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.scoreGot < n2.scoreGot) {
            return 1;
          }

          if (n1.scoreGot > n2.scoreGot) {
            return -1;
          }

          return 0;
        });
        return;
      }
    }
    if (type == "date") {
      this.sortedColumn = type;
      if (this.sortDirection == "asc") {
        this.sortDirection = "des";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.created > n2.created) {
            return 1;
          }

          if (n1.created < n2.created) {
            return -1;
          }

          return 0;
        });
        return;
      }
      if (this.sortDirection == "des") {
        this.sortDirection = "asc";
        this.sortedScores = this.sortedScores.sort((n1, n2) => {
          if (n1.created < n2.created) {
            return 1;
          }

          if (n1.created > n2.created) {
            return -1;
          }

          return 0;
        });
        return;
      }
    }
  }
}

interface Hero
{
  id: number;
  name: string;
}

interface Event
{
  id: number;
  name: string;
}

interface Score
{
  id: number;
  hero: Hero;
  event: Event;
  scoreGot: number;
  created: Date;
}
