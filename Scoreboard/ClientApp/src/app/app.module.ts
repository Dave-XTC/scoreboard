import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { HeroManagmentList } from './hero-managment/list/list.component';
import { HeroManagmentEdit } from './hero-managment/edit/edit.component';
import { HeroManagmentAdd } from './hero-managment/add/add.component';
import { HeroManagmentDelete } from './hero-managment/delete/delete.component';
import { RunManagementAdd } from './run-management/add/add.component';
import { RunManagementList } from './run-management/list/list.component';
import { RunManagementEdit } from './run-management/edit/edit.component';
import { RunManagementDelete } from './run-management/delete/delete.component';
import { RunManagementWeapons } from './run-management/weapons/weapons.component';
import { AdministrationComponent } from './administration/administration.component';
import { ScoreManagementAdd } from './score-management/add/add.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    HeroManagmentList,
    HeroManagmentEdit,
    HeroManagmentAdd,
    HeroManagmentDelete,
    RunManagementAdd,
    RunManagementList,
    RunManagementEdit,
    RunManagementDelete,
    RunManagementWeapons,
    AdministrationComponent,
    ScoreManagementAdd
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'administration', component: AdministrationComponent },
      { path: 'hero-managment/list', component: HeroManagmentList },
      { path: 'hero-managment/add', component: HeroManagmentAdd },
      { path: 'hero-managment/delete/:id', component: HeroManagmentDelete },
      { path: 'hero-managment/edit/:id', component: HeroManagmentEdit },
      { path: 'run-management/add', component: RunManagementAdd },
      { path: 'score-management/add', component: ScoreManagementAdd },
      { path: 'run-management/list', component: RunManagementList },
      { path: 'run-management/edit/:id', component: RunManagementEdit },
      { path: 'run-management/delete/:id', component: RunManagementDelete },
      { path: 'run-management/weapons', component: RunManagementWeapons },
      { path: 'fetch-data', component: FetchDataComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
