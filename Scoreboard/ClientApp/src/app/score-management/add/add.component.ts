import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'score-management-add',
  templateUrl: './add.component.html'
})
export class ScoreManagementAdd {
  score: Score;
  heroes: Hero[];
  events: Event[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string)
  {
    http.get<Hero[]>(baseUrl + 'heroes').subscribe(result => { this.heroes = result; }, error => console.error(error));
    http.get<Event[]>(baseUrl + 'events').subscribe(result => { this.events = result; }, error => console.error(error));
    this.score = {} as Score;
  }

  addScore(heroid: string, eventid: string, score: number)
  {
    var heroidN = parseInt(heroid);
    var eventidN = parseInt(eventid);
    this.score.heroid = heroidN;
    this.score.eventid = eventidN;
    this.score.scoregot = score;
    this.http.post<Score>(this.baseUrl + 'scores', this.score,).subscribe();
  }
}

interface Hero
{
  id: number;
}

interface Event
{
  id: number;
}

interface Score
{
  id: number;
  heroid: number;
  eventid: number;
  scoregot: number;
  created: Date;
}
