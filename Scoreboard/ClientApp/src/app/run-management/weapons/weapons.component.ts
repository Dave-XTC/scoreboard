import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'run-management-weapons',
  templateUrl: './weapons.component.html'
})
export class RunManagementWeapons {

  newWeapon: Weapon;
  newAttachment: Attachment;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string)
  {
    this.newWeapon = {} as Weapon;
    this.newAttachment = {} as Attachment;
  }

  addWeapon(name: string, type: string) {
    this.newWeapon.name = name;
    this.newWeapon.type = type;
    this.http.post<Weapon>(this.baseUrl + 'weapons', this.newWeapon).subscribe();
  }

  addAttachment(name: string) {
    this.newAttachment.name = name;
    this.http.post<Attachment>(this.baseUrl + 'attachments', this.newAttachment).subscribe();
  }
}

interface Weapon
{
  id: number;
  name: string;
  type: string;
}

interface Attachment {
  id: number;
  name: string;
}


