import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'run-management-edit',
  templateUrl: './edit.component.html'
})
export class RunManagementEdit {
  scores: Score[];
  run: Event;
  id: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private _Activatedroute: ActivatedRoute) {
    this.id = this._Activatedroute.snapshot.paramMap.get("id");

    const params = new HttpParams().set('id', this.id);

    http.get<Event>(baseUrl + 'events', { params }).subscribe(result => { this.run = result; }, error => console.error(error));
    http.get<Score[]>(baseUrl + 'scores').subscribe(result => { this.scores = result; }, error => console.error(error));
  }

  editRun() {
    this.http.patch<Event>(this.baseUrl + 'events', this.run).subscribe();
  }

  filterScoresByRun(): Score[] {
    let filteredScores = [];
    for (var score of this.scores) {
      if (score.event.id == this.run.id) {
        filteredScores.push(score);
      }
    }
    return filteredScores.reverse() as Score[];
  }
}

interface Event
{
  id: number;
  name: string;
  weapons: string;
  attachments: string;
  mode: string;
  rules: string;
}

interface Hero
{
  id: number;
  name: string;
  created: Date;
}

interface Score
{
  name: string;
  hero: Hero;
  event: Event;
  score: number;
  created: Date;
}
