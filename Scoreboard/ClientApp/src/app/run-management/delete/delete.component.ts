import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'run-management-delete',
  templateUrl: './delete.component.html'
})
export class RunManagementDelete {
  run: Event;
  id: string;


  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private _Activatedroute: ActivatedRoute) {
    this.id = this._Activatedroute.snapshot.paramMap.get("id");

    const params = new HttpParams().set('id', this.id);

    http.get<Event>(baseUrl + 'events', { params }).subscribe(result => { this.run = result;}, error => console.error(error));

  }

  deleteRun() {
    const params = new HttpParams().set('id', this.id);
    this.http.delete<Event>(this.baseUrl + 'events', { params }).subscribe();
  }
}

interface Event {
  id: number;
}
