import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'run-management-add',
  templateUrl: './add.component.html'
})
export class RunManagementAdd {
  run: Event;
  weapons: Weapon[];
  attachments: Attachment[];
  listWeapon: string;
  listAttachment: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string)
  {
    http.get<Weapon[]>(baseUrl + 'weapons').subscribe(result => { this.weapons = result; }, error => console.error(error));
    http.get<Attachment[]>(baseUrl + 'attachments').subscribe(result => { this.attachments = result; }, error => console.error(error));
    this.run = {} as Event;
    this.listWeapon = "" as string;
    this.listAttachment = "" as string;
  }

  addRun(name: string, weapons: string, attachments: string, mode: string, rules: string) {
    this.run.name = name;
    this.run.weapons = weapons;
    this.run.attachments = attachments;
    this.run.mode = mode;
    this.run.rules = rules;
    this.http.post<Event>(this.baseUrl + 'events', this.run,).subscribe();
  }

  addWeapon(weapon: Weapon,quan: number)
  {
    if (weapon.name != "")
    {
      if (quan > 1)
      {
        this.listWeapon = this.listWeapon.concat(quan + " * ");
      }
      this.listWeapon = this.listWeapon.concat(weapon + "\n");
    }    
  }

  delWeapon()
  {
    let list = [];
    list = this.listWeapon.split("\n");
    list.splice(list.length-2,1);
    list = list;
    this.listWeapon = list.join("\n");
  }

  addAttachment(attachment: Attachment) {
    if (attachment.name != "")
    {
      this.listAttachment = this.listAttachment.concat(attachment.name + "\n");
    }
  }

  listWeaponsByType(type: string): Weapon[]
  {
    let weaponsByType = [];
    for (var i of this.weapons)
    {
      if (i.type == type)
      {
        weaponsByType.push(i);
      }
    }
    return weaponsByType as Weapon[];
  }
}

interface Event {
  name: string;
  weapons: string;
  attachments: string;
  mode: string;
  rules: string;
}

interface Weapon
{
  id: number;
  name: string;
  type: string;
}

interface Attachment {
  id: number;
  name: string;
}


