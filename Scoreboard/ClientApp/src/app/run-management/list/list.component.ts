import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'run-management-list',
  templateUrl: './list.component.html'
})
export class RunManagementList {
  runList: Run[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string)
  {
    http.get<Run[]>(baseUrl + 'events').subscribe(result => { this.runList = result; }, error => console.error(error));
  }

}

interface Run {
  name: string;
  id: number;
  weapons: string;
  attachments: string;
  mode: string;
  rules: string;
  created: Date;
}
