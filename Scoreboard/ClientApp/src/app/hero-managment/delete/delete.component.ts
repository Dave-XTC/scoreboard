import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'hero-managment-delete',
  templateUrl: './delete.component.html'
})
export class HeroManagmentDelete {
  hero: Hero;
  heroID: string;


  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private _Activatedroute: ActivatedRoute) {
    this.heroID = this._Activatedroute.snapshot.paramMap.get("id");

    const params = new HttpParams().set('id', this.heroID);

    http.get<Hero>(baseUrl + 'heroes', { params }).subscribe(result => {this.hero = result;}, error => console.error(error));

  }

  deleteHero() {
    const params = new HttpParams().set('id', this.heroID);
    this.http.delete<Hero>(this.baseUrl + 'heroes', { params }).subscribe();
  }
}

interface Hero {
  id: number;
  name: string;
  created: Date;
}
