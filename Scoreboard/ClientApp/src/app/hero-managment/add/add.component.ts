import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'hero-management-add',
  templateUrl: './add.component.html'
})
export class HeroManagmentAdd {
  input: string;
  hero: Hero;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.hero = {} as Hero;
  }

  addHero(input: string) {
    this.hero.name = input;
    this.http.post<Hero>(this.baseUrl + 'heroes', this.hero,).subscribe();
  }
}

interface Hero {
  name: string;
}
