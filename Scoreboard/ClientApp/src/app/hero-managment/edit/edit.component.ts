import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'hero-management-edit',
  templateUrl: './edit.component.html'
})
export class HeroManagmentEdit {
  scores: Score[];
  hero: Hero;
  id: string;


  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private _Activatedroute: ActivatedRoute) {
    this.id = this._Activatedroute.snapshot.paramMap.get("id");

    const params = new HttpParams().set('id', this.id);

    http.get<Hero>(baseUrl + 'heroes', { params }).subscribe(result => { this.hero = result; }, error => console.error(error));
    http.get<Score[]>(baseUrl + 'scores',).subscribe(result => { this.scores = result; }, error => console.error(error));

  }

  updateHero()
  {
    this.http.patch<Hero>(this.baseUrl + 'heroes', this.hero).subscribe();
  }

  filterScoresByHero(): Score[]
  {
    let filteredScores = [];
    for (var score of this.scores)
    {
      if (score.hero.id == this.hero.id)
      {
        filteredScores.push(score);
      }
    }
    return filteredScores.reverse() as Score[];
  }
}

interface Hero {
  id: number;
  name: string;
  created: Date;
}

interface Score {
  id: number;
  hero: Hero;
  event: Event;
  scoreGot: number;
  created: Date;
}
