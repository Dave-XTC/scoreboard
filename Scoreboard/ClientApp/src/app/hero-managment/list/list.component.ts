import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'hero-management-list',
  templateUrl: './list.component.html'
})
export class HeroManagmentList {
  heroes: Hero[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string)
  {
    http.get<Hero[]>(baseUrl + 'heroes').subscribe(result => { this.heroes = result; }, error => console.error(error));
  }
}

interface Hero {
  id: number;
  name: string;
  created: Date;
}
